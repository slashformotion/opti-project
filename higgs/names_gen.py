
import functools


t = {
    0: "a",
    1: "b",
    2: "c",
    3: "d",
    4: "e",
    5: "f",
    6: "g",
    7: "h",
    8: "i",
    9: "k",
}

@functools.cache
def trans_char(i):
    """turn 1 to "a", 2 to "b" """
    assert isinstance(i, (int, str))
    if isinstance(i, str):
        i = int(i)
    return t[i]

@functools.cache
def trans_coord(n) -> str:
    # print(type(n), isinstance(n, int))
    assert isinstance(n, int)
    return "".join(
        [trans_char(c) for c in str(n)]
    )


@functools.cache
def edge_name(start, stop) -> str:
    return trans_coord(start) + 'z' + trans_coord(stop)

# def edge_name(start, stop) -> str:
#     return str(start) + 'z' + str(stop)

if __name__ == "__main__":
    print(ord("a"))
    print(trans_char(1))
    print(trans_char(2))
    print(trans_coord(1234567890))
