import dataclasses
from typing import Dict, List
import names_gen


@dataclasses.dataclass
class Node:
    num: int
    outwards: list[int]
    inwards: list[int]

    def start(self) -> str:
        res = " + ".join(
            [
                names_gen.edge_name(self.num, out_num) 
                for out_num in self.outwards
            ]
        )
        return f"// Node START {self.num}\n" + res + " = 1"

    def end(self) -> str:
        res = " + ".join(
            [names_gen.edge_name(in_num, self.num) for in_num in self.inwards]
        )
        return f"// Node END {self.num}\n" + res + " = 1"
    def mid(self):
        out = "".join(
            [
               " + " + names_gen.edge_name(self.num, out_num)
                for out_num in self.outwards
            ]
        )
        in_ = "".join(
            [" - " + names_gen.edge_name(in_num, self.num) for in_num in self.inwards]
        )
        return f"// Node {self.num}\n" + in_ +  out + "= 0"



if __name__ == "__main__":
    n = Node(6, [1, 4], [2])
    print(n.start())
    print(n.end())
    print(n.mid())
