import warnings
warnings.filterwarnings('ignore')
import csv
import names_gen

def create_matrix(filename):
    with open(filename, newline='') as f:
        my_list = [list(map(int,rec)) for rec in csv.reader(f, delimiter=' ')]
    return my_list


from typing import Dict, List

from node import Node

def create_map(edges: List[List[int]]) -> Dict[int, Node]:
    res = {}
    for e in edges:
        fro = e[0]
        to = e[1]
        if not  fro in res.keys():
            res[fro] = Node(num =fro ,outwards=[to], inwards=[])
        else:
            res[fro].outwards.append(to)
        if not  to in res.keys():
            res[to] = Node(num=to ,outwards=[], inwards=[fro])
        else:
            res[to].inwards.append(fro)
    return res

original = "higgs-social_network.edgelist-1"
test_file = "test_sample"
short = "higgs-short"
short2 = "higgs-short2"

def create_min_statement(edges) -> str:
    statement = "min: "
    statement += " + ".join([names_gen.edge_name(e[0], e[1])   for e in edges ])
    return statement

def create_bin_list(edges) -> str:
    statement = "bin "
    statement += ", ".join([names_gen.edge_name(e[0], e[1])  for e in edges ])
    return statement


def parse(filename: str, start: int, stop: int) -> str:
    edges = create_matrix(filename)
    print("retreived data from file")
    nodes = create_map(edges)
    print("finished creating map Dict[int, Node]")
    min_statement = create_min_statement(edges)
    print("finished creating min statement")
    out = f"// from {start} to {stop}\n"
    out += "\n"
    out  += min_statement
    out += ";\n"
    out += "\n"
    out += nodes[start].start() + ";\n"
    out += nodes[stop].end() + ";\n"
    for num, node in nodes.items():
        if num in (start, stop):
            continue
        out += node.mid() + ";\n"
    print("finished creating constraints")

    out += create_bin_list(edges) + ";\n"
    print("finished creating variable list")

    with open(f"{filename}.lp","w") as f: f.write(out)
    print("finished writing file")
    # return out
        

parse(original, 100,107)
