Description: Design and implement a parser that produces a file formulation of a general linear programming problem for a given input. Verify the resulting file formulation with lp_solve, an example of the input files is provided (the file formats are compatible with the Python Pandas library, for example, read matrix A with pd.read_csv ('matA.csv'). To_numpy ()). The recommended programming language is Python 3. The input data is in the attached files.

Input: adjacency matrix of graph A, right vector w, vector weights of criterion functions w

Result: LinearProgram.lp file formulation



