import csv
import argparse
from typing import List
from pprint import pprint
# A = [[1, 2, 1], 
#      [1, 3, 2]]


# b = [[4],
#      [8]]

# w = [[3],[5], [6]]


# """
# max:  3 x1 + 5x2 + 6 * x3;

# x1 + 2 *x2 +x3 <= 4;
# x1 + 3 *x2 +2*x3 <= 8;

# """

class SizeError(RuntimeError):
    pass

def create_matrix(filename):
    output = []
    with open(filename, newline='') as f:
        my_list = [list(map(float,rec)) for rec in csv.reader(f, delimiter=',')]
    for row in my_list:
        output_row = []
        for elem in row:
            if elem%1 ==0 :
                output_row.append(int(elem))
            else:
                output_row.append(elem)
        output.append(output_row)
    return output



def check(A, b, w):
    # check if w is of shape (1, size_vec_x)
    size_vec_x = len(w)
    for i, row in enumerate(w):
        if len(row) != 1 :
            raise SizeError(f"raw {i+1} of w should be of lenght 1 (found {len(row)})")

    # check if b is of shape (1, size_vec_constraint)
    for i, b_elem in enumerate(b):
        if len(b_elem) != 1:
            raise SizeError(f"raw {i+1} of b should be of lenght 1 (found {len(b_elem)})")
    size_vec_constraint = len(b)
    
    if len(A) != size_vec_constraint:
        raise SizeError(f"A should have {size_vec_x} rows (found {len(A)})")
    for row in A:
        if len(row) != size_vec_x:
            raise SizeError(f"raw {i+1} of A should be of lenght {size_vec_constraint} (found {len(row)})")

def build(A, b, w, slack=False, letter="x"):
    # print("A", A)
    # print("b", b)
    # print("w", w)
    check(A, b, w)

    output = "// Objective function\n"
    output += "max: "
    
    output += " + ".join([f"{w_row[0]} {letter}{i+1}" for i, w_row in enumerate(w)]) + ";"
    output += "\n\n"
    output += "// Constraints\n"
    constraints = []
    for y in range(len(A)):
        terms = []
        for x in range(len(A[0])):
            terms.append(
            f"{A[y][x]} {letter}{x+1}"
        )
        if slack != False:
            constraints.append(" + ".join(terms)+ f" + {letter}{y+1}_slack = {b[y][0]}" + ";")
        else:
            constraints.append(" + ".join(terms)+ f" <= {b[y][0]}" + ";")

    output += "\n".join(constraints) + "\n"

    output += "\n// Non-negativity\n"
    for i in range(len(w)):
        output += f"{letter}{i+1} > 0;\n"
    return output 



if __name__ == "__main__":
    parser = argparse.ArgumentParser(
                    prog='lp_parser',
                    description='This program parse 3 csv files as inputs and output a lp problem to the standard output or the out_path flag value',
                    epilog='')
    parser.add_argument('afile', help="A matrix file")           # positional argument
    parser.add_argument('bfile', help="b vector file")           # positional argument
    parser.add_argument('wfile', help="w vector file")           # positional argument
    parser.add_argument('-s', '--slack',
                    action='store_true', help="enable slack variables")  # on/off flag
    # parser.add_argument('-d', '--debug',
    #                 action='store_true', help="print the matrix to standard output and quit")  # on/off flag
    parser.add_argument('-o', '--out_path',
                    action='store', help="output to out_path if provided") 
    parser.add_argument('-l', '--letter',
                    action='store', help="set the letter of the variable in the problem")  
    args = parser.parse_args()
    try:
        A, b ,w = create_matrix(args.afile), create_matrix(args.bfile), create_matrix(args.wfile)
    except FileNotFoundError as e:
        print(e)
        exit(1)
    out = ""
    try: 
        if args.letter == None:
            out= build(A, b, w, slack=args.slack)
        else:
            out= build(A, b, w, slack=args.slack, letter=args.letter)
    except SizeError as e:
        print("SizeError:", e)
        exit(1)
    if args.out_path == None: # output to terminal
        print(out)
    else: 
        with open(args.out_path, "x") as f :
            f.write(out)
        print(f"run:\n\t lp_solve {args.out_path}")

