\documentclass{article}

\usepackage{amsmath, amsthm, amssymb, amsfonts}
\usepackage{thmtools}
\usepackage{graphicx}
\usepackage{setspace}
\usepackage{geometry}
\usepackage{float}
\usepackage{hyperref}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{framed}
\usepackage[dvipsnames]{xcolor}
\usepackage{tcolorbox}
\usepackage[cache=false]{minted}
\usemintedstyle{borland}
\usepackage{lastpage}
\usepackage{fancyhdr}
\usepackage{fancyvrb}
\pagestyle{fancy}
\fancyhead[C]{}
% \fancyhead[L]{\includegraphics[width=3.2cm]{img/logoorness.png}}
\fancyhead[R]{\includegraphics[width=3.2cm]{img/logo_uni_along.png}}
\fancyfoot[L]{\href{mailto:t8roos@enib.fr}{t8roos@enib.fr}}
\fancyfoot[C]{Linear Programming Parser Project}
\fancyfoot[R]{\thepage~/\pageref{LastPage}}
\setlength{\headheight}{45pt}
\newtheorem*{problem}{Problem}
\newtheorem*{remark}{Remark}
% Load and Make Glossaries
\usepackage[toc]{glossaries}
\makeglossaries


\title{Linear Programming Parser Project}
\author{Théophile Roos}
\date{}
% ------------------------------------------------------------------------------

\begin{document}
\newglossaryentry{oidcgloss}{
	name={Open Id Connect},
	description={OpenID Connect est une simple couche d'identification basée sur le protocole OAuth 2.0, qui autorise les clients à vérifier l'identité d'un utilisateur final en se basant sur l'authentification fournie par un serveur d'autorisation, en suivant le processus d'obtention d'une simple information du profil de l'utilisateur final. Ce processus est basé sur un dispositif interopérable de type REST.\@ En termes techniques, OpenID Connect spécifie une interface HTTP RESTful, en utilisant JSON comme format de données (source~\citep{oidcwikipedia})}
}

\newacronym{lpp}{LPP}{Linear Programming problem}
\newacronym{cli}{CLI}{Command Line Interface}

% ------------------------------------------------------------------------------
% Cover Page and ToC
% ------------------------------------------------------------------------------

\begin{figure}
	\centering

	\includegraphics[width=0.5\textwidth]{img/logo_uni.jpg}
\end{figure}
\maketitle
\begin{center}
	% \large{\textbf{14 Juillet 2022 au 18 Novembre 2022}}\\[0.2in]
	\large{ \emph{Optimization in telecommunications.}}\\[0.3in]
\end{center}
\vspace{150pt}


\large{Under the supervision of: prof. dr. Andrej Košir (andrej.kosir@lucami.fe.uni-lj.si)}
\maketitle
\newpage

\tableofcontents
\printglossaries{}
\newpage

% ------------------------------------------------------------------------------

\section{Introduction}

This is the form of a \gls{lpp} as defined during class.

\begin{equation} \label{eq:lp_def}
	\begin{aligned}
		\text{argmin} \quad & \omega_1 x_1 + \dots + \omega_n x_n \\
		\text{s.t. } \quad  &
		\begin{array}{c}
			A_{11} x_1 + \dots + A_{1n} x_n \leq b_1 \\
			\vdots                                   \\
			A_{m1} x_1 + \dots + A_{mn} x_n \leq b_m
		\end{array}
	\end{aligned}
\end{equation}

We define a linear programming problem (see \eqref{eq:lp_def}) with 1 matrix ($A$) and 2 vectors ($b$ and $\omega$) (see \eqref{eq:lp_def_matrix_form}).

\begin{equation}
	\label{eq:lp_def_matrix_form}
	\begin{aligned}
		\text{argmin} \quad & \sum_{i}^{} \omega^T_i x_i \\
		\text{s.t. } \quad  &
		\sum_{i}^{} \sum_{j}^{} A_{ij} x_i \leq b_i
	\end{aligned}
\end{equation}

\section{The example}

I found a handout of Berkeley University from \emph{Math 464 (Spring 2007)} via a simple google search: \url{https://www.ocf.berkeley.edu/~dbk/ieor162/Handout_AMPL_1.pdf}.
I suppose it is not meant to be public because \url{https://www.ocf.berkeley.edu} is the website of the "Open Computing Facility (OCF), an all-volunteer student organization dedicated to free computing".
But anyhow, the examples in that document are interesting, i will use one of them as an example throughout this report.\\

I picked:

\begin{problem}
Farmer Jones must decide how many acres of corn and wheat to plant this year.
An acre of wheat yields 25 bushels of wheat and requires 10 hours of labor per week.
An acre of corn yields 10 bushels of corn and requires 4 hours of labor per week. Wheat can be sold at 4 dollars per bushel, and corn at 3 dollars per bushel. Seven acres of land and 40 hours of labor per week are available. Government regulations require that at least 30 bushels of corn need to be produced in each week. Formulate and solve an LP that maximizes the total revenue made by Farmer Jones.
\end{problem}

\pagebreak

This problem can be modelized as follow:

% max Z = 30 x1 + 100 x2 (total revenue)
% # s.t x1 + x2 <= 7 (land available)
% # 4 x1 + 10 x2 <= 40 (labor hrs)
% # 10 x1 >= 30 (min corn)
% # x1, x2 >= 0 (non-negativity)
\begin{equation}
	\begin{aligned}
		\text{argmax} \quad & 30 y_1 + 100 y_2        & \text{(total revenue)}  \\
		\text{s.t. } \quad  &                                                   \\
		                    & y_1 +  y_2  \leq 7      & \text{(land available)} \\
		                    & 4 y_1 + 10 y_2  \leq 40 & \text{(labor hrs)}      \\
		                    & 10 y_1   \geq 30        & \text{(min corn)}       \\
		                    & y_1 , y_2  \geq 0       & \text{(non-negativity)} \\
	\end{aligned}
\end{equation}

But it is not formulated as we want right now.
First we need to get rid of the "$ >$".

\begin{equation}
	\begin{aligned}
		\text{argmax} \quad & 30 y_1 + 100 y_2       \\
		\text{s.t. } \quad  &                        \\
		                    & y_1 +  y_2   \leq 7    \\
		                    & 4 y_1 + 10 y_2 \leq 40 \\
		                    & -10 y_1   < -30        \\
		                    & y_1 , y_2  \geq 0      \\
	\end{aligned}
\end{equation}

Since we are using real variables, there is practically no difference between $>$ and $\geq$ because the resolution of the real numbers is good.

I have used $y_1$ and $y_2$ instead of $x_1$ and $x_2$ because i want to use the dual form propertie to transform the $\text{argmax}$ to a $\text{argmin}$. I can deduce that:

\begin{equation}
	\begin{aligned}
		A_{2\times3}
		=
		\left[ {\begin{array}{cc}
						        1   & 1  \\
						        4   & 10 \\
						        -10 & 0  \\
					        \end{array} } \right]
		 & \quad
		\vec{b}_{1\times3} =
		\left[ {\begin{array}{cc}
						        7   \\
						        40  \\
						        -30 \\
					        \end{array} } \right]
		 & \quad
		\vec{\omega^T}_{2\times1} =
		\left[ {\begin{array}{cc}
						        30 & 100 \\
					        \end{array} } \right]
	\end{aligned}
\end{equation}

Then follow:

\begin{equation}
	\begin{aligned}
		A_{2\times3}
		=
		\left[ {\begin{array}{cc}
						        1   & 1  \\
						        4   & 10 \\
						        -10 & 0  \\
					        \end{array} } \right]
		 & \quad
		\vec{b}_{1\times3} =
		\left[ {\begin{array}{cc}
						        7   \\
						        40  \\
						        -30 \\
					        \end{array} } \right]
		 & \quad
		\vec{\omega}_{1\times2} =
		\left[ {\begin{array}{cc}
						        30 \\ 100 \\
					        \end{array} } \right]
	\end{aligned}
\end{equation}



\pagebreak
\section{Building the parser}

I build the parser in python, but it could have been built is another language just as easily.
Here is the block diagram of the core of the system:

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{img/system_simple.png}
	\caption{Inputs and output of  the core of the system}
\end{figure}

Now it is time to choose the interface of the program. Again we have a wide variety of options. Depending of the habits of the user one choice may be better than another one. I have 2 criterias that i think are important for now:

\begin{itemize}
	\item Quickess of developpment
	\item Easy portability (eg: copy the script from an online source, no heavy installer)
\end{itemize}


\begin{remark}
	I am probably gonna work in software engineering in the futur and open-source development is one of my hobbys, so I feel compelled to make the following statement: \emph{this is a quick and dirty script}. Do not use this for production use. I would recommend to rewrite this project it in a more "secure" and robust language such as rust for exemple.
\end{remark}

I build a \gls{cli} using only libraries from the standard library, to make sure that wherever python is installed, the script will run.
\pagebreak

In the figure \ref{fig:system}, we can see the inputs in details:
\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{img/system.png}
	\caption{Inputs and output of the system}\label{fig:system}
\end{figure}


\begin{itemize}
	\item IN matA.csv: the file for the $A$ matrix.
	\item IN vecW.csv: the file for the $\omega$ vector.
	\item IN vecb.csv: the file for the $b$ vector.
	\item IN has-slack-var: boolean signaling wether we want slack variables or not.
	\item IN letter: optional string to replace $x$ in the output.
	\item IN out-path: optional string to signal the program to output the formated \gls{lpp} to the out-path instead of the standard output \\(equivalent to \verb+python lp_parse.py ... > out-path +).
\end{itemize}

The program has a nice \gls{cli} that is easily discoverable by using the \verb+--help+ flag:

\begin{listing}[H]
	\inputminted{shell}{out_prog}
	\caption{The help text of the program}
\end{listing}

\pagebreak
\section{Getting back to our problem}

Lets create the files we need:

\begin{figure}[h]
	\centering
	\begin{minipage}{.3\textwidth}
		\begin{verbatim}
    // matA.csv
    1,1
    4,10
    -10,0
  \end{verbatim}
	\end{minipage}
	\hfill
	\begin{minipage}{.3\textwidth}
		\begin{verbatim}
    // vecB.csv
    7
    40
    -30
  \end{verbatim}
	\end{minipage}
	\hfill
	\begin{minipage}{.3\textwidth}
		\begin{verbatim}
    // vecW.csv
    30
    100
  \end{verbatim}
	\end{minipage}
\end{figure}

Then call \verb+python lp_parse.py matA.csv vecB.csv vecW.csv -o farmerjones.lp+, that will create a lp problem using our input data and output a lp file called \verb+farmerjones.lp+.
Finally call \verb+lp_solve farmerjones.lp+:

\begin{figure}[h]
	\centering
	\begin{verbatim}
    Value of objective function: 370.00000000

    Actual values of the variables:
    x1                              3
    x2                            2.8
  \end{verbatim}
\end{figure}

Farmer Jones will get 370 dollars out of his seven acres of land and 40 hours of labor per week if he grow 3 acres of wheat and 2.8 acres of corn.

\section{Using our program on a bigger problem}

I found an article from atlantic press: \url{https://www.atlantis-press.com/article/125921281.pdf}

%&

% ------------------------------------------------------------------------------
% Reference and Cited Works
% ------------------------------------------------------------------------------

\newpage
\bibliographystyle{IEEEtran}
\bibliography{doc.bib}

% ------------------------------------------------------------------------------

\end{document}