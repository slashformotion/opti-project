
#set page(header: 
    grid(columns: (1fr,1fr,1fr),
        [],[],align(right, image("img/logo_uni_along.png", width: 70%))
    ),
    footer: locate(loc => {
            let page_number = counter(page).at(loc).first()
            let total_pages = counter(page).final(loc).last()
            if page_number != 1 {
              text(
                12pt,
                grid(columns: (1fr, 2fr, 1fr), 
                  align(horizon + left,link("mailto:t8roos@enib.fr")),
                  align(horizon + center, text(size: 11pt, "Linear Programming Parser Project")),
                  align(horizon + right)[ #page_number/#total_pages]
                )
              )
            }
    }),
)
#set text(size: 12pt)

#set heading(numbering: "1.1.1.1.1")

#align(center,
    [
        #image("img/logo_uni.jpg", width: 70%)
        
        #text(size: 25pt)[Linear Programming Parser Project]
        
        #text(size: 20pt)[Théophile Roos]
        
        #text(size: 20pt)[_Optimization in telecommunications._]
        #v(1fr)
        
        Under the supervision of: prof. dr. Andrej Košir (#link("mailto:andrej.kosir@lucami.fe.uni-lj.si"))
    ]
)
// END FIRST PAGE

#pagebreak()
#outline()
#v(1fr)

= Glossary


/ CLI: Command Line Interface
/ LPP: Linear Programming Problem
#v(1fr)

#pagebreak()


= Introduction

This is the form of a Linear Programming problem (LPP) as defined during class.

#grid(columns: (1fr,2fr), row-gutter: 7pt, column-gutter: 5pt,
    align(right)[argmin], [$Z = omega_1 x_1 + ... +  omega_n x_n $],
    [], [$A_11 x_1 + ... +  A_1n x_n $],
    align(right)[s.t.], [...],
    [], [$A_(m 1) x_1 + ... +  A_(m n) x_n$],
)

We define a linear programming problem  with 1 matrix (A) and 2 vectors
(b and $omega$) .

$
"argmin" Z = sum_i omega_i^T x_i
$
$
"s.t." sum_i sum_j A_(i j) x_i <= b_i
$

= The Example
