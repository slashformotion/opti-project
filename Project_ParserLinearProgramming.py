# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 11:58:10 2020

@author: Andrej Košir
"""

import numpy as np
import pandas as pd

# Input data
A = [[1, 2, 1], 
     [1, 3, 2]]


b = [[4],
     [8]]

w = [[3],[5], [6]]


"""
max:  3 x1 + 5x2 + 6 * x3

x1 + 2 *x2 +x3 <= 4
x1 + 3 *x2 +2*x3 <= 8

"""

# Save to file
A_df = pd.DataFrame(A)
A_df.to_csv('matA.csv', index=False, header=False)
b_df = pd.DataFrame(b)
b_df.to_csv('vecB.csv', index=False, header=False)
w_df = pd.DataFrame(w)
w_df.to_csv('vecW.csv', index=False, header=False)
pd.from_cs
A1 = pd.read_csv('matA.csv').to_numpy()


