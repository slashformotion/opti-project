#!/usr/bin/env python
""" lpgen_2d LP test: A m+n constraints, m*n vars, density 2/(m+n) """

from __future__ import division, print_function  
import numpy as np
from scipy import sparse as sp

__version__ = "2019-04-23 april  denis-bz-py"  # sparse

#...............................................................................
def lpgen_2d( m, n, sparse=True, dtype=np.float32, seed=0, cint=10, verbose=1 ):
    """ -> A b c LP test: A (m+n constraints, m*n vars), density 2/(m+n)
        row sums == n/m, col sums == 1
        c random integers if cint / random exponential
        sparse: linprog method="interior-point" only
    """
        # test-linprog-2d.py m=1000 sparse: float32 float64 40m 56m, both 90 sec
    m = int(m)
    n = int(n)
    random = seed if isinstance(seed, np.random.RandomState) \
            else np.random.RandomState( seed=seed )
    if cint > 0:
        c = - random.randint( 0, cint+1, size=(m,n) ).astype(dtype)
    else:
        c = - random.exponential( size=(m,n) ).astype(dtype)
    zeros = np.zeros if not sparse \
        else sp.lil_matrix
    Arow = zeros(( m, m*n ), dtype=dtype )
    Acol = zeros(( n, m*n ), dtype=dtype )
    brow = np.zeros( m, dtype=dtype )
    bcol = np.zeros( n, dtype=dtype )
    for j in range(m):
        j1 = j + 1
        Arow[ j, j*n : j1*n ] = 1
        brow[j] = n / m
    for j in range(n):
        Acol[ j, j::n ] = 1
        bcol[j] = 1
    if not sparse:
        A = np.vstack(( Arow, Acol ))
        nbytes = A.nbytes
    else:
        A = sp.vstack(( Arow, Acol )).tocsc() 
        nbytes = A.data.nbytes + A.indices.nbytes + A.indptr.nbytes
            # float32 ~ 8+4 bytes per val, float64 ~ 8+8
    b = np.hstack(( brow, bcol ))
    c = c.reshape(-1)
    if verbose:
        sparsedense = "sparse" if sparse else "dense"
        print( "lpgen_2d: m %d  n %d  A %s %.3g bytes %s %s  seed %d  -c %s ..." % (
                m, n, A.shape, nbytes, sparsedense, dtype.__name__, seed, - c[:10] ))
    return A, b, c


def _ints( x ):
    return np.asanyarray(x) .round().astype(int)

#...............................................................................
if __name__ == "__main__":
    import sys

    np.set_printoptions( threshold=20, edgeitems=10, linewidth=140,
            formatter = dict( float = lambda x: "%.2g" % x ))  # float arrays %.2g

    m = 222
    n = 422
    sparse = True
    seed = 2

    # to change these params, run this.py  a=1  b=None  c='expr' ...
    # in sh or ipython --
    for arg in sys.argv[1:]:
        exec( arg )

    A, b, c = lpgen_2d( m, n, sparse=sparse, seed=seed, verbose=2 )
    print(A,b,c)

